jQuery(document).ready(function ($) {
  if ($(".collapsible-title").length) {
    var anchor = window.location.hash;
    var collapseTitle = $(
      ".paragraphs-item-collapsible-paragraphs > .content > .collapsible-title"
    );

    $(".collapsible-title, .hfc-open-all, .hfc-close-all").attr(
      "role",
      "button"
    );
    $(
      ".collapsible-title, .hfc-open-all, .hfc-close-all, .collapsible-mm"
    ).attr("tabindex", "0");
    $(".collapsible-title").attr("aria-expanded", "false");
    $(collapseTitle).addClass("expand-plus");
    $(".collapsible-title.collapsible-mm").addClass(
      "expand-arrow collapsible-metamajor"
    );
    $(
      '<div class="collapsible-buttons"><span class="hfc-open-all" role="button" tabindex="0">Open All</span><span class="hfc-close-all" role="button" tabindex="0">Close All</span></div>'
    ).insertBefore(".collapsible-title:first");

    $(".paragraphs-items .content a").click(function () {
      var anchor = $(this).attr("hash").substr(1);
      var collapseTitleAnchor = $(
        ".paragraphs-item-collapsible-paragraphs[id=" +
          anchor +
          "] > .content > .collapsible-title"
      );

      if ($(collapseTitleAnchor).hasClass("expand-plus")) {
        $(collapseTitleAnchor).toggleClass("expand-plus collapse-minus");
        $(collapseTitleAnchor)
          .next(".hfc-show-content")
          .css("display", "block");
        $(collapseTitleAnchor).attr("aria-expanded", "true");
      }
    });

    $(function () {
      $(".paragraphs-item-collapsible-paragraphs").each(function () {
        if (anchor !== "" && $(this).attr("id") !== "") {
          var anchor = window.location.hash.substr(1);
          var collapseTitleAnchor = $(
            ".paragraphs-item-collapsible-paragraphs[id=" +
              anchor +
              "] > .content > .collapsible-title"
          );
          if ($(collapseTitleAnchor).hasClass("expand-plus")) {
            $(collapseTitleAnchor).toggleClass("expand-plus collapse-minus");
            $(collapseTitleAnchor)
              .next(".hfc-show-content")
              .css("display", "block");
            $(collapseTitleAnchor).attr("aria-expanded", "true");
          }
        }
      });
    });

    function offsetAnchor() {
      if (anchor !== "") {
        setTimeout(function () {
          window.scrollTo(window.pageXOffset, window.pageYOffset - 50);
        }, 1);
      }
    }

    window.addEventListener("hashchange", offsetAnchor);
    offsetAnchor();

    /* Open/close all buttons */
    $(".hfc-open-all").click(function () {
      $(".hfc-show-content").css("display", "block");
      if ($(collapseTitle).hasClass("expand-plus")) {
        $(collapseTitle).addClass("collapse-minus");
        $(collapseTitle).removeClass("expand-plus");
      }
      if ($(".collapsible-title.collapsible-mm").hasClass("expand-arrow")) {
        $(".collapsible-title.collapsible-mm").addClass("collapse-arrow");
        $(".collapsible-title.collapsible-mm").removeClass("expand-arrow");
      }
      $(".collapsible-title").attr("aria-expanded", "true");
    });

    $(".hfc-close-all").click(function () {
      $(".hfc-show-content").css("display", "none");
      if ($(collapseTitle).hasClass("collapse-minus")) {
        $(collapseTitle).addClass("expand-plus");
        $(collapseTitle).removeClass("collapse-minus");
      }
      if ($(".collapsible-title.collapsible-mm").hasClass("collapse-arrow")) {
        $(".collapsible-title.collapsible-mm").addClass("expand-arrow");
        $(".collapsible-title.collapsible-mm").removeClass("collapse-arrow");
      }
      $(".collapsible-title").attr("aria-expanded", "false");
    });

    $(".hfc-open-all").keypress(function (evt) {
      var e = evt || event;
      var code = e.keyCode || e.which;

      if (
        e.which === 13 ||
        e.which === 32 ||
        e.keyCode === 13 ||
        e.keyCode === 32
      ) {
        $(".hfc-show-content").css("display", "block");
        if ($(collapseTitle).hasClass("expand-plus")) {
          $(collapseTitle).addClass("collapse-minus");
          $(collapseTitle).removeClass("expand-plus");
        }
        if ($(".collapsible-title.collapsible-mm").hasClass("expand-arrow")) {
          $(".collapsible-title.collapsible-mm").addClass("collapse-arrow");
          $(".collapsible-title.collapsible-mm").removeClass("expand-arrow");
        }
        return FALSE;
      }
    });

    $(".hfc-close-all").keypress(function (evt) {
      var e = evt || event;
      var code = e.keyCode || e.which;

      if (
        e.which === 13 ||
        e.which === 32 ||
        e.keyCode === 13 ||
        e.keyCode === 32
      ) {
        $(".hfc-show-content").css("display", "none");
        if ($(collapseTitle).hasClass("collapse-minus")) {
          $(collapseTitle).addClass("expand-plus");
          $(collapseTitle).removeClass("collapse-minus");
        }
        if ($(".collapsible-title.collapsible-mm").hasClass("collapse-arrow")) {
          $(".collapsible-title.collapsible-mm").addClass("expand-arrow");
          $(".collapsible-title.collapsible-mm").removeClass("collapse-arrow");
        }
        return FALSE;
      }
    });

    /* change aria-expanded and toggle classes/display when clicked */

    $(collapseTitle).click(function () {
      if ($(this).hasClass("expand-plus")) {
        $(this).toggleClass("expand-plus collapse-minus");
        $(this).next(".hfc-show-content").css("display", "block");
      } else {
        $(this).toggleClass("expand-plus collapse-minus");
        $(this).next(".hfc-show-content").css("display", "none");
      }

      $(this).attr("aria-expanded", function (i, attr) {
        return attr == "true" ? "false" : "true";
      });
    });

    $(".collapsible-title.collapsible-mm").click(function () {
      if ($(this).hasClass("expand-arrow")) {
        $(this).toggleClass("expand-arrow collapse-arrow");
        $(this).next(".hfc-show-content").css("display", "block");
      } else {
        $(this).toggleClass("expand-arrow collapse-arrow");
        $(this).next(".hfc-show-content").css("display", "none");
      }

      $(this).attr("aria-expanded", function (i, attr) {
        return attr == "true" ? "false" : "true";
      });
    });

    /* Keyboard events - show/hide content */

    $(collapseTitle).keypress(function (evt) {
      var e = evt || event;
      var code = e.keyCode || e.which;

      if (
        e.which === 13 ||
        e.which === 32 ||
        e.keyCode === 13 ||
        e.keyCode === 32
      ) {
        if ($(this).hasClass("expand-plus")) {
          $(this).toggleClass("expand-plus collapse-minus");
          $(this).next(".hfc-show-content").css("display", "block");
        } else {
          $(this).toggleClass("expand-plus collapse-minus");
          $(this).next(".hfc-show-content").css("display", "none");
          $(this).next(".hfc-show-content").css("display", "none");
        }

        $(this).attr("aria-expanded", function (i, attr) {
          return attr == "true" ? "false" : "true";
        });

        return FALSE;
      }
    });

    /** Metamajor keyboard event **/

    $(".collapsible-title.collapsible-mm").keypress(function (evt) {
      var e = evt || event;
      var code = e.keyCode || e.which;

      if (
        e.which === 13 ||
        e.which === 32 ||
        e.keyCode === 13 ||
        e.keyCode === 32
      ) {
        if ($(this).hasClass("expand-arrow")) {
          $(this).toggleClass("expand-arrow collapse-arrow");
          $(this).next(".hfc-show-content").css("display", "block");
        } else {
          $(this).toggleClass("expand-arrow collapse-arrow");
          $(this).next(".hfc-show-content").css("display", "none");
          $(this).next(".hfc-show-content").css("display", "none");
        }

        $(this).attr("aria-expanded", function (i, attr) {
          return attr == "true" ? "false" : "true";
        });

        return FALSE;
      }
    });
  }
});
