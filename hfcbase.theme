<?php

/**
 * @file
 * Functions to support theming in the futuredriven theme.
 */

/**
 * Implements hook_preprocess_HOOK() for html.html.twig.
 */
function hfcbase_preprocess_html(array &$variables) {

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['attributes']['class'][] = 'two-sidebars';
  }
  elseif (!empty($variables['page']['sidebar_first'])) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-first';
  }
  elseif (!empty($variables['page']['sidebar_second'])) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-second';
  }
  else {
    $variables['attributes']['class'][] = 'no-sidebars';
  }

  // Set a class if the current page was created by a view.
  $route_name = \Drupal::routeMatch()->getRouteName();
  if (preg_match('/^view\./', (string) $route_name)) {
    $variables['attributes']['class'][] = 'is-views-page';
  }

  // Add classes from field_body_classes.
  if ((preg_match('/^entity\.node\.canonical$/', (string) $route_name))) {
    $node = \Drupal::request()->attributes->get('node');
    if (!empty($node->field_body_classes->value)) {
      $body_classes = explode(' ', $node->field_body_classes->value);
      foreach ($body_classes as $class) {
        $variables['attributes']['class'][] = $class;
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for paragraphs-items.html.twig.
 */
function hfcbase_preprocess_paragraph(array &$variables) {

  if (
    $variables['paragraph']->parent_type->value == 'paragraph' &&
    isset($variables['paragraph']->getParentEntity()->field_para_classes->value)
  ) {
    $parent_classes = $variables['paragraph']->getParentEntity()->field_para_classes->value;
    if (preg_match('/flex-grid/', $parent_classes)) {
      $variables['attributes']['class'][] = 'grid-child';
    }
  }
  if (!empty($variables['paragraph']->field_para_classes->value)) {
    $para_classes = explode(' ', $variables['paragraph']->field_para_classes->value);
    foreach ($para_classes as $class) {
      $variables['attributes']['class'][] = $class;
    }
  }

  if (!empty($variables['paragraph']->field_para_nested_anchor->value)) {
    $variables['attributes']['id'] = $variables['paragraph']->field_para_nested_anchor->value;
  }

  if (!empty($variables['paragraph']->field_background_photo)) {
    $path = $variables['paragraph']->field_background_photo->entity->createFileUrl();
    $inline_style = "background-image: url('{$path}')";
    $variables['attributes']['style'] = $inline_style;
  }
}
